import {
  SearchItem,
  isEntity,
  isCompendiumEntity,
  DocumentType,
  CompendiumSearchItem,
} from "./searchLib";
import { isTextInputElement, TextInputElement } from "./utils";
import { settings, getSetting } from "./settings";
import type { SearchFilter } from "./searchFilters";
import type { Editor as TinyMCEEditor } from "tinymce";

declare let ui: { windows: Application[] };

export enum ContextMode {
  Browse,
  Insert,
}

export abstract class SearchContext {
  mode?: ContextMode = ContextMode.Insert;
  spawnCSS?: Record<string, string | number> = {};
  classes?: [];
  filter?: string | SearchFilter;
  startText?: string;
  allowMultiple? = true;
  restrictTypes?: DocumentType[];
  abstract onSubmit(item: SearchItem | string): void;
  onClose(): void {
    return;
  }
}

// Default browse context
export class BrowseContext extends SearchContext {
  constructor() {
    super();
    this.startText = document.getSelection()?.toString();
  }
  mode: ContextMode = ContextMode.Browse;
  onSubmit(item: SearchItem): void {
    // Render the sheet for selected item
    item.show();
  }
}

export class InputContext extends SearchContext {
  input: TextInputElement;
  selectionStart: number | null = null;
  selectionEnd: number | null = null;

  constructor(input: TextInputElement) {
    super();
    this.input = input;
    const targetRect = input.getBoundingClientRect();
    const bodyRect = document.body.getBoundingClientRect();
    const top = targetRect.top - bodyRect.top;
    // TODO: Real calculation!!!
    this.spawnCSS = {
      left: targetRect.left + 5,
      bottom: bodyRect.height - top - 30,
      width: targetRect.width - 10,
    };
    this.selectionStart = input.selectionStart;
    this.selectionEnd = input.selectionEnd;

    if (this.selectionStart !== null && this.selectionEnd !== null) {
      if (this.selectionStart != this.selectionEnd) {
        this.startText = this.input.value.slice(
          this.selectionStart,
          this.selectionEnd
        );
      }
    }

    $(input).addClass("quick-insert-context");
  }
  insertResult(result: string): void {
    if (this.selectionStart !== null && this.selectionEnd !== null) {
      this.input.value =
        this.input.value.slice(0, this.selectionStart) +
        result +
        this.input.value.slice(this.selectionEnd);
    } else {
      this.input.value = result;
    }
  }
  onSubmit(item: SearchItem | string): void {
    if (typeof item == "string") {
      this.insertResult(item);
    } else {
      this.insertResult(item.journalLink);
    }
  }
  onClose(): void {
    $(this.input).removeClass("quick-insert-context");
    this.input.focus();
  }
}

export class ScriptMacroContext extends InputContext {
  onSubmit(item: SearchItem | string): void {
    if (typeof item == "string") {
      this.insertResult(`"${item}"`);
    } else {
      this.insertResult(item.script);
    }
  }
}

export class RollTableContext extends InputContext {
  allowMultiple = false;
  constructor(input: TextInputElement) {
    super(input);
    // Set filter depending on selected dropdown!
    // const resultRow = this.input.closest("li.table-result")
  }
  onSubmit(item: SearchItem | string): void {
    if (typeof item == "string") {
      this.insertResult(item);
      return;
    }

    const row = $(this.input).closest(".table-result");
    const resultId = row.data("result-id");
    const appId = row.closest(".window-app").data("appid");
    const app = ui.windows[parseInt(appId)] as DocumentSheet;

    if (isEntity(item)) {
      app.object.updateEmbeddedDocuments("TableResult", [
        {
          _id: resultId,
          collection: item.documentType,
          type: 1,
          resultId: item.id,
          text: item.name,
          img: item.img || null,
        },
      ]);
    } else if (isCompendiumEntity(item)) {
      app.object.updateEmbeddedDocuments("TableResult", [
        {
          _id: resultId,
          collection: item.package,
          type: 2,
          resultId: item.id,
          text: item.name,
          img: item.img || null,
        },
      ]);
    }
  }
}

export class TinyMCEContext extends SearchContext {
  editor: TinyMCEEditor;
  constructor(editor: TinyMCEEditor) {
    super();

    const targetRect = editor.selection.getBoundingClientRect();
    const bodyRect = document.body.getBoundingClientRect();
    const containerRect = editor.contentAreaContainer.getBoundingClientRect();
    const top = containerRect.top + targetRect.top;

    this.spawnCSS = {
      left: containerRect.left + targetRect.left,
      bottom: bodyRect.height - top - 20,
      width: targetRect.width,
      maxHeight: top + 20,
    };
    this.editor = editor;
  }
  onSubmit(item: SearchItem | string): void {
    if (typeof item == "string") {
      this.editor.insertContent(item);
    } else {
      this.editor.insertContent(item.journalLink);
    }
  }
  onClose(): void {
    this.editor.focus();
  }
}

export class CharacterSheetContext extends SearchContext {
  documentSheet: DocumentSheet;
  anchor: JQuery<HTMLElement>;

  restrictTypes = [DocumentType.ITEM];

  constructor(documentSheet: DocumentSheet, anchor: JQuery<HTMLElement>) {
    super();
    this.documentSheet = documentSheet;
    this.anchor = anchor;

    const targetRect = anchor.get()[0].getBoundingClientRect();
    const bodyRect = document.body.getBoundingClientRect();
    const top = bodyRect.top + targetRect.top;

    this.spawnCSS = {
      left: targetRect.left - 280,
      bottom: bodyRect.height - top - 23,
      width: 300,
      maxHeight: top + 23,
    };
  }
  onSubmit(item: SearchItem | string): void {
    if (typeof item == "string") return;
    //@ts-ignore
    return this.documentSheet._onDropItem(
      {},
      {
        id: item.id,
        type: item.documentType,
        ...(item instanceof CompendiumSearchItem && { pack: item.package }),
      }
    );
  }
}

export function identifyContext(target: Element): SearchContext | null {
  if (target && isTextInputElement(target)) {
    if (target.name === "command") {
      if (
        (
          target
            .closest(".macro-sheet")
            ?.querySelector('select[name="type"]') as HTMLSelectElement
        )?.value === "script"
      ) {
        return new ScriptMacroContext(target);
      }
      return new InputContext(target);
    } else if (
      target.name.startsWith("results.") &&
      target.closest(".result-details")
    ) {
      return new RollTableContext(target);
    }

    // Right now, only allow in chat!
    if (target.id === "chat-message") {
      return new InputContext(target);
    }
  }

  // No/unknown context, browse only.
  if (getSetting(settings.ENABLE_GLOBAL_CONTEXT) === true) {
    return new BrowseContext();
  }
  return null;
}

export class EmbeddedContext extends BrowseContext {
  spawnCSS = {
    top: "unset",
    left: "0",
    bottom: "0",
    "max-height": "100%",
    width: "100%",
    "box-shadow": "none",
  };
  onSubmit(): void {
    return;
  }
}
