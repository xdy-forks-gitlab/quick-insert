import {
  CompendiumSearchItem,
  DocumentAction,
  EntitySearchItem,
  DocumentType,
  IndexedDocumentTypes,
  documentIcons,
  getCollectionFromType,
  SearchItem,
} from "./searchLib";

export const DOCUMENTACTIONS: Record<string, (item: SearchItem) => any> = {
  show: (item) => item.show(),
  roll: (item) => item.get().then((d) => d.draw()),
  viewScene: (item) => item.get().then((d) => d.view()),
  activateScene: (item) =>
    item.get().then((d) => {
      game.user?.isGM && d.activate();
    }),
  execute: (item) => item.get().then((d) => d.execute()),

  insert: (item) => item,
  rollInsert: (item) =>
    item.get().then(async (d) => {
      const roll = d.roll();
      for (const res of roll.results) {
        if (res.resultId == "") {
          return res.text;
        }

        if (res.collection.includes(".")) {
          if (!game.packs) return;
          const pack = game.packs.get(res.collection);
          if (!pack) return;
          const indexItem = game.packs
            .get(res.collection)
            ?.index.find((i) => i._id === res.resultId);
          return indexItem && new CompendiumSearchItem(pack, indexItem);
        } else {
          const entity = getCollectionFromType(res.collection).get(
            res.resultId
          );
          return entity ? new EntitySearchItem(entity as any) : null;
        }
      }
    }),
};

export const BrowseDocumentActions: Record<string, DocumentAction[]> = (() => {
  const actions: Record<string, DocumentAction[]> = {
    [DocumentType.SCENE]: [
      {
        id: "activateScene",
        icon: "fas fa-bullseye",
        title: "Activate",
      },
      {
        id: "viewScene",
        icon: "fas fa-eye",
        title: "View",
      },
      {
        id: "show",
        icon: "fas fa-cogs",
        title: "Configure",
      },
    ],
    [DocumentType.ROLLTABLE]: [
      {
        id: "roll",
        icon: "fas fa-dice-d20",
        title: "Roll",
      },
      {
        id: "show",
        icon: `fas ${documentIcons[DocumentType.ROLLTABLE]}`,
        title: "Edit",
      },
    ],
    [DocumentType.MACRO]: [
      {
        id: "execute",
        icon: "fas fa-play",
        title: "Execute",
      },
      {
        id: "show",
        icon: `fas ${documentIcons[DocumentType.ROLLTABLE]}`,
        title: "Edit",
      },
    ],
  };
  IndexedDocumentTypes.forEach((type) => {
    if (type in actions) return;
    actions[type] = [
      {
        id: "show",
        icon: `fas ${documentIcons[type]}`,
        title: "Show",
      },
    ];
  });
  return actions;
})();

// Same for all inserts
const insertAction = {
  id: "insert",
  icon: `fas fa-plus`,
  title: "Insert",
};

export const InsertDocumentActions: Record<DocumentType, DocumentAction[]> =
  (() => {
    const actions: Record<string, DocumentAction[]> = {
      [DocumentType.SCENE]: [
        {
          id: "show",
          icon: "fas fa-cogs",
          title: "Configure",
        },
      ],
      [DocumentType.ROLLTABLE]: [
        {
          id: "rollInsert",
          icon: "fas fa-play",
          title: "Roll and Insert",
        },
        {
          id: "show",
          icon: `fas ${documentIcons[DocumentType.ROLLTABLE]}`,
          title: "Show",
        },
      ],
    };

    // Add others
    IndexedDocumentTypes.forEach((type) => {
      if (!actions[type]) {
        // If nothing else, add "Show"
        actions[type] = [
          {
            id: "show",
            icon: `fas ${documentIcons[type]}`,
            title: "Show",
          },
        ];
      }
      actions[type].push(insertAction);
    });
    return actions;
  })();

export function getActions(
  type: DocumentType,
  isInsertContext: boolean
): DocumentAction[] {
  return isInsertContext
    ? InsertDocumentActions[type]
    : BrowseDocumentActions[type];
}

export function defaultAction(
  type: DocumentType,
  isInsertContext: boolean
): string {
  const actions = getActions(type, isInsertContext);
  return actions[actions.length - 1].id;
}
