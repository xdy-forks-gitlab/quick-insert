export interface SystemIntegration {
  id: string;
  init(): void;
  defaultSheetFilters?: { [key: string]: string };
}

export async function importSystemIntegration(): Promise<
  SystemIntegration | undefined
> {
  let system = null;
  switch (game.system.id) {
    case "dnd5e":
      system = await import("../systems/dnd5e");
      break;
    case "pf2e":
      system = await import("../systems/pf2e");
      break;
    case "swade":
      system = await import("../systems/swade");
      break;
    case "wfrp4e":
      system = await import("../systems/wfrp4e");
      break;
    case "sfrpg":
      system = await import("../systems/sfrpg");
      break;
    default:
      return;
  }

  return {
    id: game.system.id,
    ...system,
  };
}
