# Quick Insert - Find and insert entities you need

Quick Insert provides a context-aware search/autocomplete tool that can be used
in most parts of FoundryVTT. Find actors, items, spells etc. and insert them
wherever you need them right now - the map, character sheets, roll tables,
journals, you name it.

## Background

Quick Insert is inspired by autocomplete/intellisense and smarter search
interfaces, such as search engines, command pallettes, browser address bars etc.

## Usage

Open the Quick Insert window with

- the keyboard shortcut <kbd>Ctrl</kbd> + <kbd>Space</kbd> (editable in
  settings)
- the Quick Insert button in rich-text editors
- the Quick Insert button in select character sheets

If you focus a supported input field, Quick Insert will automatically attach to
that input in Insert-mode. (currently supported: chat, rolltables, macro editor
and rich text editors)

Quick Insert can work in two slightly different modes - the context-aware
**"Insert-mode"** and a general **"Browse-mode"**

### Using the search result:

- Use arrow keys <kbd>↑</kbd><kbd>↓</kbd> or mouse to navigate the search
  results
- Select with click or <kbd>Enter</kbd>; **submit** in Insert-mode, or **open**
  in Browse-mode.
- Hold <kbd>Shift</kbd> to submit/open without closing the search, so you can
  select more than one!
- Drag-drop directly from the search results
- **Use search filters:** start by typing `@` and select the filter with
  <kbd>Tab</kbd>, <kbd>Enter</kbd> or click

## Examples

[Album with more examples](https://imgur.com/a/mHv88KW)

![Drag-drop things](https://i.imgur.com/sFTBYQw.gif)
![Insert in character sheets](https://i.imgur.com/PfaoB3g.gif)
![Use filters](https://i.imgur.com/ORegNUd.gif)
![Insert in character sheet](https://i.imgur.com/PfaoB3g.gif)
![Filter editor](https://i.imgur.com/bYBExRy.png)

## Installation

Quick Insert is available in the in-application module browser. It can also be
installed manually with the following manifest URL:

```
https://gitlab.com/fvtt-modules-lab/quick-insert/-/jobs/artifacts/master/raw/module.json?job=build-module
```

### Unstable branch

If you want to live on the edge, you can use the latest version from the
`develop` branch, but there may be unfinished features and rough edges.

```
https://gitlab.com/fvtt-modules-lab/quick-insert/-/jobs/artifacts/develop/raw/module.json?job=build-unstable
```

## Search sources

Quick Insert lets you search both among entities in your world, as well as
entities in any of your compendiums. Search for **Actors, Items, Journals,
Macros, Rollable Tables or Scenes**.

You can configure who is allowed to search for which type of entities, as well
as in which compendiums by using the "Indexing settings" menu.

## Integrated search context - Insert mode

Quick Insert can attach itself to a certain **"context"** for automatic
insertion. As of now, these are:

- Journals and other TinyMCE(rich text) editors, via keyboard shortcut or Quick
  Insert button in the tool bar - supports quick search of selected text!
- Macro editor (both script and chat macros)
- Chat (Note: @ tags may conflict with mentions when it's in the start of a
  message)
- Rollable table editor - automatically updates entire row when searching in
  text field
- Dnd5e and Tidy5e character and NPC sheets have search buttons added
- Modules may spawn a Quick Insert prompt with a custom context (PM me on
  Discord if you want help to integrate)

## Developers & API

Modules are free to invoke Quick Insert, although incompatible changes may occur
without notice, breaking compatibility.

### `QuickInsert.open(context)`

Open the Quick Insert search app, with an optional context to attach to. If you
add a "context" to the Quick Insert search app, you can control how you want the
search to behave.

```JavaScript
QuickInsert.open({
  // All fields are optional
  spawnCSS: {
    // This entire object is passed to JQuery .css()
    // https://api.jquery.com/css/#css-properties
    left: 600,
    top: 100,
  };
  classes: [ "myClass" ],  // A list of classes to be added to the search app
  filter: "dnd5e.items",   // The name of a filter to pre-fill the search
  startText: "bla",        // The search input will be pre-filled with this text
  allowMultiple: true,     // Unless set to `false`, the user can submit multiple times using the shift key
  restrictTypes: ["Item"], // Restrict the output to these document types only
  onSubmit: (item) => {
    // Do something with the selected item, e.g.
    item.show();
  },
  onClose: () => {
    // You can do something when the search app is closed
  }
})
```

### `QuickInsert.search(text, filter = null, max = 100)`

Use the internal search library from Quick Insert to look up some search
results.

### `QuickInsert.forceIndex()`

Forces Quick Insert to rebuild the entire search index. Useful for testing.

### `QuickInsert.matchBoundKeyEvent(event)`

Match the user-bound keyboard event manually. For integrating with custom text editors.

### `QuickInsert.hasIndex`

True if QuickInsert has an index that can be searched. Gives no guarantees that compendiums have been loaded yet.

### `SearchItem`

The object passed to `onSubmit`, as well as the items in the result from
`QuickInsert.search()` is called `SearchItems`. They have the following
properties:

```JavaScript
item.name
item.id
item.img
item.documentType
// Computed properties
// Get the uuid value compatible with fromUuid
item.uuid
// Get the draggable attributes in order to make custom elements
item.draggableAttrs
// Get the html for an icon that represents the item
item.icon
// Get a clickable (preferrably draggable) link to the document
item.link
// Reference the document in a journal, chat or other places that support it
item.journalLink
// Reference the document in a script
item.script
// Short tagline that explains where/what this is
item.tagline
// (async) Show the sheet or equivalent of this search result
item.show()
// Fetch the original object (or null if no longer available).
// NEVER call as part of indexing or filtering.
// It can be slow and most calls will cause a request to the database!
// Call it once a decision is made, do not call for every SearchItem!
item.get()
```

### Hooks

#### `QuickInsert:IndexCompleted`

Called when the index (including compendiums) have been (re-)built.

### "Debug mode"

You can force QuickInsert into "debug mode" by setting
`QuickInsert.app.debug = true`.

- Quick Insert will stay open even when it loses focus.
- More debug features may be added in the future.

## System and character sheet integration

While developers can use the API themselves to control Quick Insert, there is
also some integration built into Quick Insert. If you have a system or a certain
character sheet variant, and want Quick Insert integration, feel free to create
a new issue here on GitLab.

## Known issues

- The compendium search index is built on page load. If something is missing -
  refresh the page

### Performance in Firefox

Search performance in Firefox degrades on longer search inputs. This is an issue
with Firefox and the fuzzy search library used by Quick Insert -
[Fuse.js](https://fusejs.io/).
